const studentDetailTable = document.querySelector(".table-section");

const getStudentDetails = (event) => {

    const selectedClass = event.target.className;
    const selectedId = Number.parseInt(event.target.id);

    const fetchStudentDetail = async () => {
        try {
            const response = await fetch('http://cc83-103-51-153-190.ngrok.io/fetchAllStudent', {

                method: 'GET',
                headers: new Headers({
                    "ngrok-skip-browser-warning": "1234",

                })
            });

            const data = await response.json();
            openStudentDetailForm(data, selectedClass, selectedId);

        } catch (error) {
            console.log(error)
        }
    }

    fetchStudentDetail()
}

studentDetailTable.addEventListener("click", () => getStudentDetails(event));

const openStudentDetailForm = (data, selectedClass, selectedId) => {
    for (let studentData of data) {
        console.log(data)
        if (selectedClass === "edit-button" && selectedId === studentData.enrollmentNumber) {

            studentDetailForm();
            
            document.getElementById("enrollment-number").value = studentData.enrollmentNumber
            document.getElementById("first-name").value = studentData.firstName;
            document.getElementById("last-name").value = studentData.lastName;
            document.getElementById("gender").value = studentData.gender;
            document.getElementById("rating").value = studentData.rating;
            document.getElementById("dob").value = studentData.dob;
            document.getElementById("stream").value = studentData.stream;
            editStudentDetails();
            const submitButton = document.querySelector(".submit-form");

            submitButton.addEventListener("click", () => editStudentDetails())
        }
    }
}

const editStudentDetails = async()=>{

    const enrollmentNumberElement = document.getElementById("enrollment-number").value;
    const firstNameElement = document.getElementById("first-name").value;
    const lastNameElement = document.getElementById("last-name").value;
    const genderElement = document.getElementById("gender").value;
    const ratingElement = document.getElementById("rating").value;
    const dobElement = document.getElementById("dob").value;
    const streamElement = document.getElementById("stream").value;
    console.log(stream);

    try {

        const response = await fetch('http://8c8b-2409-4042-4cb0-1e03-ed3d-8f9e-8cee-cc0a.ngrok.io/updateStudent', {
            // const response = await fetch('/details.json', {
            method: 'PUT',
            headers: new Headers({
                "ngrok-skip-browser-warning": "1234",
            }),
            body: JSON.stringify({
                "enrollmentNumber": `${enrollmentNumberElement}`,
                "firstName": `${firstNameElement}`,
                "lastName": `${lastNameElement}`,
                "gender": `${genderElement}`,
                "stream": `${streamElement}`,
                "rating": `${ratingElement}`,
                "dob": `${dobElement}`,
            })
        });
        
    } catch (error) {
        console.log(error)
    }
}


