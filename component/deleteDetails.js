const deleteDetails = (event) => {

    const selectedClass = event.target.className;
    console.log(selectedClass)

    if (selectedClass === 'delete-button') {

        const body = document.querySelector("body");
        const popupSection = document.createElement("section");
        popupSection.classList = "delete-popup";
        body.appendChild(popupSection);

        popupSection.innerHTML = `
                <section class="delete-popup">
                    <h3>Are you sure you want to delete</h3>
                    <button type="button" class = "delete-yes">Yes</button>
                    <button type="button" class = "delete-no">No</button>
                </section>
    `
        const deleteNoButton = document.querySelector(".delete-no");
        const deleteYesButton = document.querySelector(".delete-yes");

        deleteNoButton.addEventListener("click", () => closeDeletePopup(event));

        deleteYesButton.addEventListener("click", () => deleteStudentDetail(event));
    }
}

studentDetailTable.addEventListener("click", () => deleteDetails(event))

const deleteStudentDetail = (event) => {
    const selectedId = Number.parseInt(event.target.id);
    const deletePopup = document.querySelector(".delete-popup")
    confirmDelete(selectedId);
}

const deleteBtn = document.querySelector(".delete-button");
console.log(deleteBtn);

const confirmDelete = async (selectedId) => {
    try {
        const response = await fetch(`http://cc83-103-51-153-190.ngrok.io/deleteStudent/${selectedId}`, {
            method: 'DELETE',
            headers: new Headers({
                "ngrok-skip-browser-warning": "1234",
            })
        });
    } catch (error) {
        console.log(error)
    }
    location.reload();
}


const closeDeletePopup = (event)=>{
    const deletePopup = document.querySelectorAll(".delete-popup");
    console.log(deletePopup);
    for(let deletepop of deletePopup){
        deletepop.close();
    }
}