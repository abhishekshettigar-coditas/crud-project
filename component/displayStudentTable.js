const displayStudents = async()=>{

    try {

        const response = await fetch('http://cc83-103-51-153-190.ngrok.io/fetchAllStudent', {
          method: 'GET',
          headers: new Headers({
            "ngrok-skip-browser-warning": "1234",
          })
        });
        const data = await response.json();
        tableDetails(data);
      } catch (error) {
        console.log(error)
      }
}

displayStudents();

const tableDetails = (data)=>{

    const tableSection = document.querySelector(".table-section");

    tableSection.innerHTML = `

    <table class="table">
            <thead>

                <tr>
                    <th>Student Enrollment Number</th>
                    <th>Student first Name</th>
                    <th>Student last Name</th>
                    <th>Student gender</th>
                    <th>Student stream</th>
                    <th>Student rating</th>
                    <!-- <th>Student DOB</th> -->
                    <th>Edit / Update</th>
                </tr>

            </thead>
    </table>
    `
    const tableElement = document.querySelector(".table");

    for(studentData of data){
      
        tableElement.innerHTML += `
                <tbody>
                  <tr>
                    <td>${studentData.enrollmentNumber}</td>
                    <td>${studentData.firstName}</td>
                    <td>${studentData.lastName}</td>
                    <td>${studentData.gender}</td>
                    <td>${studentData.stream}</td>
                    <td>${studentData.rating}</td>
                    <!--<td>${studentData.dob}</td>-->
                    <td><button type="button" class = "edit-button" id="${studentData.enrollmentNumber}">Edit</button><button type="button" class = "delete-button" id = "${studentData.enrollmentNumber}">Delete</button></td>
                  </tr>
                </tbody>
                `
    }
    
}


