const addDetails = async (event) => {

    const selectedId = event.target.id;
    if(selectedId === 'submit-form'){
    console.log(selectedId);
    event.preventDefault();

    const enrollmentNumberElement = document.getElementById("enrollment-number").value;
    const firstNameElement = document.getElementById("first-name").value;
    const lastNameElement = document.getElementById("last-name").value;
    const genderElement = document.getElementById("gender").value;
    const ratingElement = document.getElementById("rating").value;
    const dobElement = document.getElementById("dob").value;
    const streamElement = document.getElementById("stream").value;
    console.log(enrollmentNumberElement)
    console.log(firstNameElement)
    const form = document.querySelector(".form-container");
    form.reset();
    location.reload();
    console.log("hello")
    try {
               
        const response = await fetch("http://cc83-103-51-153-190.ngrok.io/registerStudent", {
        // const response = await fetch('/details.json', {
            method: 'POST',
            headers: new Headers({
                "ngrok-skip-browser-warning": "1234",
                'Content-type': 'application/json; charset=UTF-8'
            }),
            body: JSON.stringify({
                "enrollmentNumber": `${enrollmentNumberElement}`,
                "firstName": `${firstNameElement}`,
                "lastName": `${lastNameElement}`,
                "gender": `${genderElement}`,
                "stream": `${streamElement}`,
                "rating": `${ratingElement}`,
                "dob": `${dobElement}`,
            })
        });
        
    }
    catch (error) {
        console.log(error);
    }
}
}

const submitButton = document.querySelector(".form-popup");
console.log(submitButton);

submitButton.addEventListener("click", () => addDetails(event));