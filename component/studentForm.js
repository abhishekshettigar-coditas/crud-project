const createButton = document.querySelector(".create-button");

const studentDetailForm = () => {

    const formPopup = document.querySelector(".form-popup");

    formPopup.innerHTML = `

    <form class="form-container" action="http://cc83-103-51-153-190.ngrok.io/registerStudent" method="POST">

    <button type="button" class="close-form" id = "close-form">&times;</button>

        <h3>Enter Student Details</h3>


        <label for="enrollment-number" class="form-fields">Enter Enrollment number: <input type="number"
        name="enrollment-number" id="enrollment-number"></label>
        
        <label for="first-name" class="form-fields">Enter Student First Name: <input type="text" name="first-name"
        id="first-name"></label>
        
        <label for="last-name" class="form-fields">Enter Student last Name: <input type="text" name="last-name"
        id="last-name">
        </label>
        
        <label for="gender" class="form-fields">Enter Student gender: <input type="text" name="gender"
        id="gender"></label>
        
        <label for="stream" class="form-fields">Enter Student stream: <input type="text" name="stream"
        id="stream"></label>
        
        <label for="rating" class="form-fields">Enter Student rating: <input type="number" name="rating"
        id="rating"></label>
        
        <label for="dob" class="form-fields">Enter Student date of birth: <input type="date" name="dob"
        id="dob"></label>

        <button type="button" class="submit-form" id = "submit-form">Submit</button>
    </form>
    `
}

createButton.addEventListener("click", () => studentDetailForm())

const form = document.querySelector(".form-popup");

const closeForm = (event)=>{
    const selectedId = event.target.id;
    if(selectedId === "close-form"){
        const formPopup = document.querySelector("form");
        formPopup.remove();
    }
}

form.addEventListener('click', () => closeForm(event))
